import {useState} from 'react';
import './App.css'
import Counter from "./components/Counter/ Counter";
import ResetButton from "./components/ResetButton/ ResetButton";

const App = () => {
    const [countOfTry, setCountOfTry] = useState([{count: 0}]);
    const [gameOn, setGameOn] = useState(true)
    const number = (max, min)=> {
        return Math.floor(Math.random() * (max - min + min) + min);
    }
    const numberIndex = number(36, 1);
    const forSetBlocks = () => {
        if (gameOn) {
            setCountOfTry(countOfTry.map(count => {
                return {
                    ...count,
                    count: 0,
                }
            }));
            const arrayDivs = [];
            for (let i = 1; i < 37; i++) {
                if (i === numberIndex) {
                    arrayDivs.push({id: i, className: 'close', symbol: 'O'});
                } else {
                    arrayDivs.push({id: i, className: 'close'});
                }
            }
            return arrayDivs;
        }
    }

    const [blocks, setBlocks] = useState(forSetBlocks);


    const classChange = id => {
        if(numberIndex === id){
            setGameOn(!gameOn)
        }
        if (gameOn) {
            setCountOfTry(countOfTry.map(count => {
                return {
                    ...count,
                    count: count.count + 1
                }
            }));
            const block = blocks.map(block => {
                if (block.id === id) {
                    return {
                        ...block,
                        className: 'open'
                    }
                }
                return block
            })
            setBlocks(block)
        }
    }

    const reset = () => {
        setBlocks(forSetBlocks);
    }

    const block = blocks.map(block => (
            <div key={block.id} className={block.className} onClick={() => classChange(block.id)}>{block.symbol}</div>
        )
    )
    return (
        <div>
            <div className="container blocks">
                {block}
            </div>
            <div className="container text">
                <Counter tryingCount={countOfTry[0].count}/>
            </div>
            <div className="container">
              <ResetButton reset={reset}/>
            </div>
        </div>
    )
}

export default App;
