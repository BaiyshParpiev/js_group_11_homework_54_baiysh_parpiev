import React from 'react';

const Counter = ({tryingCount}) => {
    const counterStyle = {color: '#61dafb', textAlign: 'center', margin: '0 auto'};

    if(tryingCount >= 10){
        counterStyle.color = 'yellow';
    }

    if(tryingCount >= 20) {
        counterStyle.color = 'red';
    }

    return (
        <p style={counterStyle}>
            You tried: {tryingCount}
        </p>
    );
};

export default Counter;