import React from 'react';

const ResetButton = ({reset}) => {
    return (
        <button className="reset" onClick={reset}>Reset</button>
    );
};

export default ResetButton;